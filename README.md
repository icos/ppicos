![](images/logo_ppicos1_256px.png)

# ppicos

**Repository was moved to GitLab: https://github.com/holukas/ppicos**

`ppicos` (**p**ost-**p**rocessing for ICOS) reads raw data files recorded at the ICOS site CH-DAV and converts
their formats to ICOS-conform file formats.

